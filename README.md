AlgoExpert - Solving Problems Using Java

Easy Questions 01

Escreva uma função que receba um array não vazio de inteiros distintos e um
inteiro representando uma soma desejada. Se quaisquer dois números na soma do array de entrada
chegar até a soma desejada, a função deve retorná-los em um array. 
Se dois números não forem somados à soma desejada, a função deve retornar
um array vazio.


Observe que a soma desejada deve ser obtida pela soma de dois números inteiros diferentes
na matriz; você não pode adicionar um único inteiro a si mesmo para obter o
soma alvo.


Você pode supor que haverá no máximo um par de números totalizando
a soma alvo.


Exemplo de Entrada:
```java
array = [3, 5, -4, 8, 11, 1, -1, 6]
targetSum = 10
```

Exemplo de Saída:
```java
[-1, 11] // os números podem estar em uma order invertida
```


> [First Solution](https://bitbucket.org/jorge_rabello/algoexpert/src/master/src/br/com/jorgerabellodev/algoexpert/easy/firstproblem/FirstSolution.java)
>
> Time: O(n^2)
>
> Space: O(1) space


> [Second Solution](https://bitbucket.org/jorge_rabello/algoexpert/src/master/src/br/com/jorgerabellodev/algoexpert/easy/firstproblem/SecondSolution.java)
>
> Time:  O(n)
>
> Space: O(n)


> [Third Solution](https://bitbucket.org/jorge_rabello/algoexpert/src/master/src/br/com/jorgerabellodev/algoexpert/easy/firstproblem/ThirdSolution.java)
>
> Time:  O(nlog(n))
>
> Space: O(1)