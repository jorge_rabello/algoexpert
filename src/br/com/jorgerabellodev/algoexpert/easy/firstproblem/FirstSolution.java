package br.com.jorgerabellodev.algoexpert.easy.firstproblem;

import java.time.Duration;
import java.time.Instant;

public class FirstSolution {
    public static void main(String[] args) {
        Instant start = Instant.now();
        int[] array = new int[] {3, 5, -4, 8, 11, 1, -1, 6};
        twoNumberSum(array, 10);
        Instant end = Instant.now();
        System.out.println("Elapsed Time: " + Duration.between(start, end).toString());
    }

    // O(n^2) time | O(1) space
    private static int[] twoNumberSum(int[] array, int targetSum) {
        for (int i = 0; i < array.length -1; i++) {
            int firstNum = array[i];
            for (int j = i + 1; j < array.length; j++) {
                int secondNumber = array[j];
                if (firstNum + secondNumber == targetSum) {
                    return new int[] {firstNum, secondNumber};
                }
            }
        }
        return new int[0];
    }
}
