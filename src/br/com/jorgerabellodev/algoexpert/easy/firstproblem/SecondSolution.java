package br.com.jorgerabellodev.algoexpert.easy.firstproblem;

import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

public class SecondSolution {
    public static void main(String[] args) {
        Instant start = Instant.now();
        int[] array = new int[] {3, 5, -4, 8, 11, 1, -1, 6};
        twoNumberSum(array, 10);
        Instant end = Instant.now();
        System.out.println("Elapsed Time: " + Duration.between(start, end).toString());
    }

    // O(n) time | O(n) space
    private static int[] twoNumberSum(int[] array, int targetSum) {
        Set<Integer> nums = new HashSet<>();
        for (int num:array) {
            int potentialMatch = targetSum - num;
            if (nums.contains(potentialMatch)) {
                return new int[] {potentialMatch, num};
            } else {
                nums.add(num);
            }
        }
        return new int[0];
    }
}