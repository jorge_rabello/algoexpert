package br.com.jorgerabellodev.algoexpert.easy.firstproblem;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

public class ThirdSolution {
    public static void main(String[] args) {
        Instant start = Instant.now();
        int[] array = new int[] {3, 5, -4, 8, 11, 1, -1, 6};
        twoNumberSum(array, 10);
        Instant end = Instant.now();
        System.out.println("Elapsed Time: " + Duration.between(start, end).toString());
    }
    // O(nlog(n)) | O(1) space
    private static int[] twoNumberSum(int[] array, int targetSum) {
        Arrays.sort(array);
        int left = 0;
        int right = array.length -1;
        while (left < right) {
            int currentSum = array[left] + array[right];
            if (currentSum == targetSum) {
                return new int[] {array[left], array[right]};
            } else if (currentSum < targetSum) {
                left++;
            } else {
                right--;
            }
        }
        return new int[0];
    }
}
